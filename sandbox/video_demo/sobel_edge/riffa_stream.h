#ifndef RIFFA_STREAM_H
#define RIFFA_STREAM_H

#include <stdlib.h>
#include <iostream>
#include "riffa.h"
#include <bitset>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <cxxabi.h>

struct maxpoolParamsType{
    int  reset  ;
    int  cflush ;
    int  Ix     ;
    int  Iy     ;
    int  Iz     ;
    int  Kx     ;
    int  Ky     ;
    int  Sx     ;
    int  Sy     ;
    int  Ox     ;
    int  Oy     ;
    bool relu   ;

};

struct convParamsType{
    int Kx;
    int Ky;
    int Sx;
    int Sy;
    int Ix;
    int Iy;
    int Iz;
    int Oz;
};


static inline void print_stacktrace(FILE *out = stderr, unsigned int max_frames = 63)
{
    fprintf(out, "stack trace:\n");

    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0) {
	fprintf(out, "  <empty, possibly corrupt>\n");
	return;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
	char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

	// find parentheses and +address offset surrounding the mangled name:
	// ./module(function+0x15c) [0x8048a6d]
	for (char *p = symbollist[i]; *p; ++p)
	{
	    if (*p == '(')
		begin_name = p;
	    else if (*p == '+')
		begin_offset = p;
	    else if (*p == ')' && begin_offset) {
		end_offset = p;
		break;
	    }
	}

	if (begin_name && begin_offset && end_offset
	    && begin_name < begin_offset)
	{
	    *begin_name++ = '\0';
	    *begin_offset++ = '\0';
	    *end_offset = '\0';

	    // mangled name is now in [begin_name, begin_offset) and caller
	    // offset in [begin_offset, end_offset). now apply
	    // __cxa_demangle():

	    int status;
	    char* ret = abi::__cxa_demangle(begin_name,
					    funcname, &funcnamesize, &status);
	    if (status == 0) {
		funcname = ret; // use possibly realloc()-ed string
		fprintf(out, "  %s : %s+%s\n",
			symbollist[i], funcname, begin_offset);
	    }
	    else {
		// demangling failed. Output function name as a C function with
		// no arguments.
		fprintf(out, "  %s : %s()+%s\n",
			symbollist[i], begin_name, begin_offset);
	    }
	}
	else
	{
	    // couldn't parse the line? print the whole line.
	    fprintf(out, "  %s\n", symbollist[i]);
	}
    }

    free(funcname);
    free(symbollist);
}

void myassert(bool condition, const char msg[]=""){
    if(!condition){
        printf("ASSERTION FAILURE:: ");
        printf("%s\n",msg);
        printf("\n");
        print_stacktrace();
        fflush(stdout);
        exit(1);
    }
}

/************************************************************************************/
/**********************LEVEL 1 (LOWEST LEVEL)****************************************/
/************************************************************************************/
class send_fifo;
class recv_fifo;
class riffa_stream{
    private:
        fpga_t *fpga;
        void make_send_cmd(unsigned int* sendBuffer,int numWords){
            sendBuffer[0] = numWords;
            sendBuffer[1] = 1;
        }

        void make_recv_cmd(unsigned int* sendBuffer,int numWords){
            sendBuffer[0] = numWords;
            sendBuffer[1] = 0;
        }

        int send(unsigned int* sendBuffer, int numWords){
            myassert(numWords%4 == 0,"send ERROR: numWords must be divisible by 4");
            make_send_cmd(sendBuffer,numWords);
            return fpga_send(fpga, 0, sendBuffer, numWords+4, 0, 1, 0)-4;

        }

        int recv(unsigned int* recvBuffer, int numWords){
            myassert(numWords%4 == 0,"recv ERROR: numWords must be divisible by 4");
            myassert(numWords != 0,"recv ERROR: numWords to fetch can not be zero");
            make_recv_cmd(recvBuffer,numWords); //use the recv buffer for copying
            //std::cout<<"Requesting "<<numWords<<" elements from FPGA\n";
            if(fpga_send(fpga, 1, recvBuffer, 4, 0, 1, 0) == 4){
                //std::cout<<"Requested "<<numWords<<" elements from FPGA\n";
                int size = fpga_recv(fpga, 1, recvBuffer, numWords, 0); 
                //std::cout<<"Received "<<size<<" elements from FPGA\n";
                return size;
            }
            else{
                std::cout<<"ERROR: Requested "<<numWords<<" elements from FPGA\n";
                exit(1);
            }
            return 0;
        }

    public:
        riffa_stream(){
            fpga = fpga_open(0);
            myassert(fpga > 0,"open ERROR: Could not open fpga 0");
        }

        ~riffa_stream(){
            fpga_close(fpga);
        }
        friend riffa_stream& operator<<(riffa_stream& fpga, send_fifo& sBuffer);
        friend riffa_stream& operator>>(riffa_stream& fpga, recv_fifo& rBuffer);
};

/************************************************************************************/
/************************************LEVEL 2*****************************************/
/************************************************************************************/
#define    CMD     0
#define    IFMAP   1
#define    KERNEL  2
#define    TXCMD   3
#define    BIAS    4

struct cmdType{
    bool  reset          ;
    bool  cflush         ;
    int   Kx             ; // 7
    int   Ky             ; // 7
    int   Sx             ; // 7
    int   Sy             ; // 7
    int   Ix             ; // 13
    int   Iy             ; // 13
    int   Iz             ; // 13
    int   Oz             ; // 13
    int   num_active_PEs ; // 13

};

void serialize_conv_cmd(
        convParamsType conv_params ,
        unsigned int* buffer       ,
        int rst                    ,
        int cflush                 ,
        int num_active_PEs){

    buffer[0] = 0;
    buffer[1] = 0;
    buffer[2] = 0;
    buffer[3] = 0;

    buffer[0] |= rst                        << 0  ;
    buffer[0] |= cflush                     << 1  ;
    buffer[0] |= conv_params.Kx             << 2  ;
    buffer[0] |= conv_params.Ky             << 9  ;
    buffer[0] |= conv_params.Sx             << 16 ;
    buffer[0] |= conv_params.Sy             << 23 ;
    buffer[0] |= conv_params.Ix             << 30 ;
    buffer[1] |= conv_params.Ix             >> 2  ;
    buffer[1] |= conv_params.Iy             << 11 ;
    buffer[1] |= conv_params.Iz             << 24 ;
    buffer[2] |= conv_params.Iz             >> 8  ;
    buffer[2] |= conv_params.Oz             << 5  ;
    buffer[2] |= num_active_PEs             << 18 ;
}

void serialize_maxpool_cmd(maxpoolParamsType command, unsigned int* buffer, int rst,int cflush){
    buffer[0]  = 0;
    buffer[1]  = 0;
    buffer[2]  = 0;
    buffer[3]  = 0;

    buffer[0] |= rst          << 0  ;
    buffer[0] |= cflush       << 1  ;
    buffer[0] |= command.Ix   << 2  ;
    buffer[0] |= command.Iy   << 15 ;
    buffer[0] |= command.Iz   << 28 ;
    buffer[1] |= command.Iz   >> 4  ;
    buffer[1] |= command.Kx   << 9  ;
    buffer[1] |= command.Ky   << 12 ;
    buffer[1] |= command.Sx   << 15 ;
    buffer[1] |= command.Sy   << 18 ;
    buffer[1] |= command.Ox   << 21 ;
    buffer[2] |= command.Ox   >> 11 ;
    buffer[2] |= command.Oy   << 2  ;
    buffer[2] |= command.relu << 15 ;
}



class send_fifo{
    private:
        std::vector<unsigned int> sendVector;
        std::vector<float> pendingIfmap;
        std::vector<float> pendingKernel;
        std::vector<float> pendingBias;
        void verify_params(convParamsType & conv_params, maxpoolParamsType & maxpool_params,int num_active_PEs){

            //printf("verify1 failed:: (Ix(%d)-Kx(%d)+1)%%Sx(%d) == 0\n",conv_params.Ix,conv_params.Kx,conv_params.Sx);
            //if((conv_params.Ix-conv_params.Kx+1)%conv_params.Sx != 0){
            //    exit(1);
            //}
            //myassert( 0 ==
            //        (conv_params.Ix-conv_params.Kx+1)%conv_params.Sx, "verify1 failed\n"                           );

            //myassert( 0 ==
            //        (conv_params.Iy-conv_params.Ky+1)%conv_params.Sy , "verify2 failed\n"                          );

            myassert( maxpool_params.Ix ==
                    (conv_params.Ix-conv_params.Kx+conv_params.Sx)/conv_params.Sx , "verify3 failed\n"                          );

            //myassert( maxpool_params.Iy ==
            //        (conv_params.Iy-conv_params.Ky+conv_params.Sy)/conv_params.Sy , "verify4 failed\n"                          );
            if(maxpool_params.Iy != (conv_params.Iy-conv_params.Ky+conv_params.Sy)/conv_params.Sy){
                printf("ERROR: maxpool_params.Iy(%d) != (conv_params.Iy(%d) -Ky(%d))/Sy(%d)+1\n",maxpool_params.Iy,conv_params.Iy,conv_params.Ky,conv_params.Sy);
                myassert(false);
            }

            myassert( maxpool_params.Ox ==
                    (maxpool_params.Ix-maxpool_params.Kx+maxpool_params.Sx)/maxpool_params.Sx , "verify5 failed\n" );

            myassert( maxpool_params.Oy ==
                    (maxpool_params.Iy-maxpool_params.Ky+maxpool_params.Sy)/maxpool_params.Sy , "verify6 failed\n" );

            myassert( conv_params.Oz ==
                    maxpool_params.Iz , "verify7 failed\n"                                                         );

            myassert( num_active_PEs ==
                    maxpool_params.Ix*maxpool_params.Iy    , "verify8 failed\n"                                    );

            /*if(num_active_PEs > NUM_PE_PER_CORE){
                printf("ERROR: num_active_PEs(%d)>NUM_PE_PER_CORE(%d)\n",num_active_PEs,NUM_PE_PER_CORE);
                exit(1);
            }*/
        }
 
        void push_ifmap128(){
            int size = pendingIfmap.size();
            int validWord;
            switch(size){
                case 1  : validWord = 8  ;
                          break          ;
                case 2  : validWord = 12 ;
                          break          ;
                case 3  : validWord = 14 ;
                          break          ;
                case 4  : validWord = 15 ;
                          break          ;
                default :
                          myassert(false,"ERROR:pendingIfmap.size() is invalid\n");
            }

            sendVector.push_back(IFMAP); //tag
            sendVector.push_back(validWord); //validWord
            for(int i=2;i<4;i++){
                sendVector.push_back(0);
            }

            for(int i=0;i<4;i++){
                pendingIfmap.push_back(0);
            }

            for(int cc=0;cc<4;cc++){
                sendVector.push_back(*((unsigned int*)(&(pendingIfmap[3-cc]))));
            }

            pendingIfmap.clear();
        }

        void push_kernel128(){
            int size = pendingKernel.size();
            int validWord;
            switch(size){
                case 1  : validWord = 8  ;
                          break          ;
                case 2  : validWord = 12 ;
                          break          ;
                case 3  : validWord = 14 ;
                          break          ;
                case 4  : validWord = 15 ;
                          break          ;
                default :
                          myassert(false,"ERROR:pendingKernel.size() is invalid\n");
            }

            sendVector.push_back(KERNEL); //tag
            sendVector.push_back(validWord); //validWord
            for(int i=2;i<4;i++){
                sendVector.push_back(0);
            }

            for(int i=0;i<4;i++){
                pendingKernel.push_back(0);
            }

            for(int cc=0;cc<4;cc++){
                sendVector.push_back(*((unsigned int*)(&(pendingKernel[3-cc]))));
            }

            pendingKernel.clear();
        }

        void push_bias128(){
            int size = pendingBias.size();
            int validWord;
            switch(size){
                case 1  : validWord = 8  ;
                          break          ;
                case 2  : validWord = 12 ;
                          break          ;
                case 3  : validWord = 14 ;
                          break          ;
                case 4  : validWord = 15 ;
                          break;
                default :
                          myassert(false,"ERROR:pendingBias.size() is invalid\n");
            }

            sendVector.push_back(BIAS); //tag
            sendVector.push_back(validWord); //validWord
            for(int i=2;i<4;i++){
                sendVector.push_back(0);
            }

            for(int i=0;i<4;i++){
                pendingBias.push_back(0);
            }

            for(int cc=0;cc<4;cc++){
                sendVector.push_back(*((unsigned int*)(&(pendingBias[3-cc]))));
            }

            pendingBias.clear();
        }

    public:

        send_fifo(){ 
            //first four bytes for riffa_stream to send its commands
            for(int i=0;i<4;i++){ 
                sendVector.push_back(0);
            }
        }

        void push_reset(){
            unsigned int rawBuffer[4];

            sendVector.push_back(CMD); //tag
            for(int i=1;i<4;i++){
                sendVector.push_back(0);
            }

            sendVector.push_back(1);
            for(int i=1;i<4;i++){
                sendVector.push_back(0);
            }
        }

        void push_cmd(convParamsType conv_params, maxpoolParamsType maxpool_params){
            unsigned int rawBuffer[4];

            int num_active_PEs = maxpool_params.Ix*maxpool_params.Iy ;
            verify_params(conv_params, maxpool_params, num_active_PEs);

            sendVector.push_back(CMD); //tag
            for(int i=1;i<4;i++){
                sendVector.push_back(0);
            }

            serialize_conv_cmd(conv_params,rawBuffer,0,0,num_active_PEs);
            for(int i=0;i<4;i++){
                sendVector.push_back(rawBuffer[i]); //conv cmd
            }

            sendVector.push_back(TXCMD); //tag
            for(int i=1;i<4;i++){
                sendVector.push_back(0);
            }

            serialize_maxpool_cmd(maxpool_params,rawBuffer,0,0);
            for(int i=0;i<4;i++){
                sendVector.push_back(rawBuffer[i]); //maxpool cmd
            }
        }

        inline void flush_pending(){
            if(pendingIfmap.size()>0){
                push_ifmap128();
            }
            if(pendingKernel.size()>0){
                push_kernel128();
            }
            if(pendingBias.size()>0){
                push_bias128();
            }
        }

        void push_ifmap(float dataF){
            if(pendingIfmap.size()==0){//to flush pending kernel/bias
                flush_pending();
            }

            pendingIfmap.push_back(dataF);

            if(pendingIfmap.size()==4){//to flush pending ifmap
                flush_pending();
            }
        }

        void push_kernel(float dataF){
            if(pendingKernel.size()==0){//to flush pending ifmap/bias
                flush_pending();
            }

            pendingKernel.push_back(dataF);

            if(pendingKernel.size()==4){//to flush pending kernel
                flush_pending();
            }
        }

        void push_bias(float dataF){
            if(pendingBias.size()==0){//to flush pending ifmap/kernel
                flush_pending();
            }

            pendingBias.push_back(dataF);

            if(pendingBias.size()==4){//to flush pending bias
                flush_pending();
            }
        }

        void push(unsigned int value){
            sendVector.push_back(value);
        }

        int size(){
            return sendVector.size()-4; //First 4 words are for riffa_stream's header
        }

        void print(){
            int nelems = sendVector.size();
            std::cout<<"[";
            for(int i = 4; i < nelems;i++){
                unsigned int* v=&sendVector[i];
                //float* vf=(float*)v;
                std::cout<< *v<<",";
            }
            std::cout<<"]\n";
        }
        void binarydump(const char filename[]){
            myassert(pendingIfmap.size()==0,"ERROR: pendingIfmap has unflushed elements.\n");
            std::ofstream file;
            file.open(filename);
            int nelems = sendVector.size();

            myassert(nelems%4==0,"ERROR in send_fifo.binarydump(): nelems must be div by 4\n");
            for(int i = 4; i < nelems;i+=4){
                for(int j=3;j>=0;j--){ //so that we have {sendVector[3],sendVector[2],sendVector[1],sendVector[0]}
                    std::bitset<32> bits(sendVector[i+j]);
                    file<<bits.to_string(); 
                }
                file<<"\n";
            }
            file.close();
        }

        friend riffa_stream& operator<<(riffa_stream& fpga, send_fifo& sBuffer);
};

class recv_fifo{
    private:
        int loc;
        int nelems;
        int _size;
    public:
        std::vector<unsigned int> recvVector;
        recv_fifo(int sz){
            resize(sz);
            loc=0;
        }

        unsigned int pop(int step=1){
                if(loc<nelems){
                    unsigned int val = *(unsigned int*)&recvVector[loc];
                    loc+=step;
		    return val;
                }
                else{
                    return -101;
                    myassert(false,"recv_fifo underflow\n");
                }
        }

        int size(){
            return _size; //First 4 words are for riffa_stream's header
        }

        void resize(int sz){
            recvVector.resize(sz);
            _size = sz;
            nelems = 0;
            loc = 0;
        }
        void shrink(int sz){
            nelems=sz;
            loc=0;
        }
        void print(){
            std::cout<<"[";
            for(int i = 0; i < nelems;i++){
                unsigned int* v=&recvVector[i];
                //float* vf=(float*)v;
                //printf("%f,",*vf);
                std::cout<< *v<<",";
            }
            std::cout<<"]\n";
        }

        friend riffa_stream& operator>>(riffa_stream& fpga, recv_fifo& rBuffer);
};

riffa_stream& operator<<(riffa_stream& fpga, send_fifo& sBuffer){
    sBuffer.flush_pending();
    int numWords = sBuffer.size();
    int sent=fpga.send(sBuffer.sendVector.data(),numWords);
    //std::cout<<"numWords="<<sent<<std::endl;
    //std::cout<<"Sent="<<sent<<std::endl;
    myassert (sent==numWords,"Failed to send all data\n");
    return fpga;
}

riffa_stream& operator>>(riffa_stream& fpga, recv_fifo& rBuffer){
    rBuffer.nelems = fpga.recv(rBuffer.recvVector.data(),rBuffer._size);
    rBuffer.loc    = 0;
    myassert(rBuffer.nelems==rBuffer._size,"Failed to fetch all data.\n"); 
    return fpga;
}
#endif
