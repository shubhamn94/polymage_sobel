#include "sobel.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <libio.h>
#include <pthread.h>
#include "/home/shubham/riffa_stream.h"
#define MAX_WIDTH  1920
#define MAX_HEIGHT 1080
void *send(void *);
void *recv(void *);
pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;

const int SIZE = 476;

extern "C" {
	#include "/home/shubham/work/sobel/jpeg-6b/jpeglib.h"
}

using namespace std;

static inline int
read_JPEG_file(char *filename,
               uint16_t *width, uint16_t *height, uint16_t *channels, unsigned char *(image[]))
{
  FILE *infile;
  if ((infile = fopen(filename, "rb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    return 0;
  }

  struct jpeg_error_mgr jerr;
  struct jpeg_decompress_struct cinfo;
  cinfo.err = jpeg_std_error(&jerr);
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, infile);
  (void) jpeg_read_header(&cinfo, TRUE);
  (void) jpeg_start_decompress(&cinfo);

  *width = cinfo.output_width, *height = cinfo.output_height;
  *channels = cinfo.num_components;
  unsigned long int i = *width * *height * *channels * sizeof(*image);
  *image = (unsigned char*)malloc(i);
  JSAMPROW rowptr[1];
  int row_stride = *width * *channels;

  while (cinfo.output_scanline < cinfo.output_height) {
    rowptr[0] = *image + row_stride * cinfo.output_scanline;
    jpeg_read_scanlines(&cinfo, rowptr, 1);
  }
  jpeg_finish_decompress(&cinfo);

  jpeg_destroy_decompress(&cinfo);
  fclose(infile);
  return 1;
}

struct fpga_comm {
    
	  riffa_stream fpga;
	  send_fifo sBuffer;
	  recv_fifo *rBuffer;
	  fpga_comm(int siz) {
		  rBuffer = new recv_fifo(siz);
	  }
};
typedef struct fpga_comm fpga_comm;

int main (int argc, char** argv) {
uint16_t ch, input, i, j, k = 0, width, height, channels, row = 1, flag, diff, max, min;
unsigned char *image;
uint16_t ROWS = 3;
uint16_t pack_dim[8], pad_src = 0;
uint16_t pad = 0;
uint16_t *imag_gray = (uint16_t*) malloc(sizeof(uint16_t)*MAX_WIDTH*MAX_HEIGHT);
uint16_t *imag_result = (uint16_t*) malloc(sizeof(uint16_t)*MAX_WIDTH*MAX_HEIGHT);

pthread_t thread1, thread2;
int rc1, rc2;

//size of data after packing including dimensions size
uint32_t size = (MAX_WIDTH * MAX_HEIGHT)/2 + 4;

//Declaring pointers for packing data and dimensions
uint32_t *pack_ptr, *packdim_ptr;

//Declaring pointers for unpacking data
uint16_t *unpack_ptr;

//Declaring temporary array for storage of packed 32 bit data
uint32_t temp_result[size];

read_JPEG_file("/home/shubham/work/sobel/HK2.jpg", &width, &height, &channels, &image);

if (channels == 3) {
  channels = 1;
  for (i = 0; i<height; i++) {
    for (j = 0; j<width; j++) {
      imag_gray[i*width+j] = ((uint16_t)image[i*width*3+j*3] + (uint16_t)image[i*width*3+j*3+1] + (uint16_t)image[i*width*3+j*3+2])/3;
    }
  }
}

printf("height=%d\nwidth=%d\n",height,width);
for (i = 0; i<height; i++) {
  for (j = 0; j<width; j++) {
	  imag_result[i*width + j] = imag_gray[i*width + j];
  }
}

i = 0;
flag = 0;
pack_ptr = (uint32_t*)imag_gray;
uint16_t* ptr = (uint16_t*)pack_ptr;

pack_dim[0] = width;
pack_dim[1] = height;
pack_dim[2] = ROWS;

packdim_ptr = (uint32_t*)pack_dim;

while(i < height) {

 if(flag == 0) { 

	 //pad_src = 2 --> Only for image width = 950. It is hard-coded.
	 pad_src = 2;
	 pack_dim[3] = pad_src;
	 fpga_comm *args = new fpga_comm(SIZE);
	 
	 args->sBuffer.push(packdim_ptr[0]);
	 args->sBuffer.push(packdim_ptr[1]);

	 for(j = 0; j < ROWS; j++) {
		 for(k = 0; k < (width/2); k++)
			 args->sBuffer.push(pack_ptr[j*(width/2) + k]);
	 }
	 for(k = 0; k < pad_src; k++)
		 pack_dim[k] = pad;
	 for(k = 0; k < (pad_src/2); k++)
		 args->sBuffer.push(packdim_ptr[k]);

	 if( (rc1=pthread_create( &thread1, NULL, &send, (void *)args)) )
		 printf("Thread creation failed: %d\n", rc1);
   
	 
	 if( (rc2=pthread_create( &thread2, NULL, &recv, (void *)args)) )
		 printf("Thread creation failed: %d\n", rc2);
   
	 
	 pthread_join(thread1, NULL);
	 pthread_join(thread2, NULL); 

	 for(j = 0; j < (width/2 + 1); j++) 
		 temp_result[j] = args->rBuffer->pop();
	  
	 unpack_ptr = (uint16_t*)temp_result;

	 for(j = 0; j < width; j++)
		 imag_result[row*width + j] = unpack_ptr[j];

	 i = ROWS;
	 flag = 1;
	 row++;
	 delete args;
 }

 else {
	 pack_dim[0] = width;
	 pack_dim[1] = height;
	 pack_dim[2] = flag;
	 
	 //pad_src = 6 --> Only for image width = 950. It is hard-coded 
	 pad_src = 6;
	 pack_dim[3] = pad_src;
	 fpga_comm *args = new fpga_comm(SIZE); 
	 
	 args->sBuffer.push(packdim_ptr[0]);
	 args->sBuffer.push(packdim_ptr[1]);

	 for(j = 0; j < width; j++) 
		 std::cout<<ptr[i*width + j] <<" ";
	 
	 std::cout<<"\nvalue of i is: "<<i<<"\n";

	 for(j = 0; j < (width/2); j++)
		args->sBuffer.push(pack_ptr[i*(width/2) + j]);

	 args->sBuffer.print();
	
	 for(k = 0; k < pad_src; k++) 
		 pack_dim[k] = pad;
	 for(k = 0; k < (pad_src/2); k++)
		 args->sBuffer.push(packdim_ptr[k]);

         if( (rc1=pthread_create( &thread1, NULL, &send, (void *)args)) )
                 printf("Thread creation failed: %d\n", rc1);


         if( (rc2=pthread_create( &thread2, NULL, &recv, (void *)args)) )
                 printf("Thread creation failed: %d\n", rc2);


         pthread_join(thread1, NULL);
         pthread_join(thread2, NULL);

	 for(j = 0; j < (width/2 + 1); j++) 
		 temp_result[j] = args->rBuffer->pop();

	 unpack_ptr = (uint16_t*)temp_result;
	 //std::cout<<"\n";
	 for(j = 0; j < width; j++) {
		 //std::cout<<unpack_ptr[j] <<" ";
		 imag_result[row*width + j] = unpack_ptr[j];
	 }

	 i++;
	 row++;
	 delete args;
 }
}

 max = imag_result[1*width + 1];
 min = max;
 for (i = 1; i < height-1; i++) {
	 for (j = 1; j < width-1; j++) {
		 if (imag_result[i*width + j]>max) max = imag_result[i*width + j];
		 if (imag_result[i*width + j]<min) min = imag_result[i*width + j];
	 }
 }

 diff = max - min;

 for (i = 1; i < height-1; i++) {
 	 for (j = 1; j < width-1; j++) {
 		 float abc = (imag_result[i*width + j]-min)/(diff*1.0);
		 imag_result[i*width + j] = abc * 255;
 	 }
 }

 FILE *output_file = fopen("/home/shubham/work/sobel/result", "w");
 if(output_file == NULL) {
	printf("Couldn't open output file \n");
	return 0;
 }

 fprintf(output_file, "%d ", (int)width);
 fprintf(output_file, "%d ", (int)height);
 for(i = 0; i < height; i++) {
	for(j = 0; j < width; j++) {
		fprintf(output_file, "%d ", (int)imag_result[i*width + j]);
	}
 }

 fclose(output_file);

 return 0;
}

 void *send(void *arguments) {
	fpga_comm *arg = (fpga_comm*)arguments; 
	printf("\nData is ready to be sent to FPGA\n");
	arg->fpga << arg->sBuffer;
	printf("\nData has been successfully sent to FPGA!! \n");
	pthread_exit(NULL);
	return NULL;
 }

 void *recv(void *arguments) {
	 fpga_comm *arg = (fpga_comm*)arguments;
	 arg->fpga >> *(arg->rBuffer);
	 printf("\nRequested data of size 476 has been successfully received from FPGA!! \n");
	 pthread_exit(NULL);
	 return NULL;
 }
